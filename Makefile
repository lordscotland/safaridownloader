ARCHS = armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = safaridownloader
safaridownloader_FILES = $(wildcard *.[mx])
safaridownloader_FRAMEWORKS = UIKit MobileCoreServices
safaridownloader_LDFLAGS = -Llibfinder -lfinder

include $(THEOS_MAKE_PATH)/tweak.mk

PREFERENCES_DIR = layout/Library/PreferenceLoader/Preferences/com.officialscheduler.safaridownloader
MKSTRINGS = strings/mkstrings
export IOS_ROOT = strings/%root

all :: $(patsubst strings/%,$(PREFERENCES_DIR)/%,$(wildcard strings/*.lproj))
$(PREFERENCES_DIR)/% : strings/% $(MKSTRINGS) ; $(MKSTRINGS) $(PREFERENCES_DIR)/entry.plist $< $@
$(MKSTRINGS) : $(MKSTRINGS).m ; gcc -framework Foundation -o $@ $<
