#import <Foundation/Foundation.h>

int main(int argc,char** argv) {
  char* rdir=getenv("IOS_ROOT");
  if(argc<4 || !rdir){return 1;}
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  NSFileManager* manager=[NSFileManager defaultManager];
  NSMutableSet* strings=[NSMutableSet set];
  for (NSDictionary* item in [[NSDictionary dictionaryWithContentsOfFile:
   [manager stringWithFileSystemRepresentation:argv[1]
   length:strlen(argv[1])]] objectForKey:@"items"]){
    NSString* str;
    if((str=[item objectForKey:@"label"])){[strings addObject:str];}
    if((str=[item objectForKey:@"footerText"])){[strings addObject:str];}
  }
  NSString* infile=[manager stringWithFileSystemRepresentation:argv[2]
   length:strlen(argv[2])],*stem=infile.lastPathComponent;
  NSDictionary* lproj=[NSDictionary dictionaryWithContentsOfFile:infile],
   *lproj1=[NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:
   @"%s/Applications/MobileSafari.app/%@/Localizable.strings",rdir,stem]],
   *lproj2=[NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:
   @"%s/System/Library/Frameworks/UIKit.framework/%@/Localizable.strings",rdir,stem]];
  NSMutableDictionary* outlproj=[NSMutableDictionary dictionary];
  for (NSString* key in strings){
    NSString* tstr=[lproj objectForKey:key]?:
     [lproj1 objectForKey:key]?:[lproj2 objectForKey:key];
    if(!tstr){fprintf(stderr,"W: Missing key [%s]\n",key.UTF8String);}
    [outlproj setObject:tstr?:key forKey:key];
  }
  NSString* outdir=[manager stringWithFileSystemRepresentation:argv[3]
   length:strlen(argv[3])];
  [manager createDirectoryAtPath:outdir withIntermediateDirectories:YES
   attributes:nil error:NULL];
  [[NSPropertyListSerialization dataWithPropertyList:outlproj
   format:NSPropertyListBinaryFormat_v1_0 options:0 error:NULL]
   writeToFile:[outdir stringByAppendingPathComponent:@"Localizable.strings"]
   atomically:YES];
  [pool drain];
  return 0;
}
