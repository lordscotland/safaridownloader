#include "SDRotatingViewController.h"

@implementation SDRotatingViewController
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation {
  return orientation!=UIInterfaceOrientationPortraitUpsideDown;
}
@end
